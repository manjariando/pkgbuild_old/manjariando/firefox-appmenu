# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Jakub Schmidtke <sjakub@gmail.com>

### Appmenu patching ###
# PKGBUILD: Nikita Tarasov <nikatar@disroot.org>

pkgname=firefox-appmenu
_pkgname=firefox
pkgver=115.0.2
pkgrel=1
pkgdesc="Firefox from extra with appmenu patch"
arch=(x86_64)
license=(MPL GPL LGPL)
url="https://www.mozilla.org/firefox"
depends=(gtk3 libxt mime-types dbus-glib ffmpeg nss ttf-font libpulse libdbusmenu-gtk3 libxss)
makedepends=(unzip zip diffutils yasm mesa imake inetutils xorg-server-xvfb
             rust clang llvm jack nodejs cbindgen nasm python
             lld dump_syms wasi-compiler-rt wasi-libc wasi-libc++ wasi-libc++abi
             )
optdepends=('networkmanager: Location detection via available WiFi networks'
            'libnotify: Notification integration'
            'pulseaudio: Audio support'
            'speech-dispatcher: Text-to-Speech'
            'hunspell-en_US: Spell checking, American English'
            'xdg-desktop-portal: Screensharing with Wayland')
provides=("firefox=$pkgver")
conflicts=("firefox")
options=(!emptydirs !makeflags !strip !lto !debug)
source=(https://archive.mozilla.org/pub/firefox/releases/$pkgver/source/firefox-$pkgver.source.tar.xz{,.asc}
        https://metainfo.manjariando.com.br/${pkgname}/org.mozilla.${pkgname}.metainfo.xml
        ${_pkgname}.desktop
        identity-icons-brand.svg
        "fix-wayland-build.patch::https://aur.archlinux.org/cgit/aur.git/plain/fix-wayland-build.patch?h=firefox-appmenu"
        "unity-menubar.patch"
        "fix_csd_window_buttons.patch::https://aur.archlinux.org/cgit/aur.git/plain/fix_csd_window_buttons.patch?h=firefox-appmenu"
        "0001-Revert-Bug-1817071-Remove-moz-image-region-support-f.patch")
sha256sums=('6b2844124c13b4bd9d97ac7f5840f3e957b7631659591f3bea1ac9a89bee1654'
            'SKIP'
            '31e381e0710a5ac76b51999863ac87f6a577aebe5ee98b8a335bd258d76eff54'
            '34514a657d6907a159594c51e674eeb81297c431ec26a736417c2fdb995c2c0c'
            'a9b8b4a0a1f4a7b4af77d5fc70c2686d624038909263c795ecc81e0aec7711e9'
            '46724a625f51c358abaee488a7ce75673078e96ba009459339120b8dd11dec25'
            '4d24dd97aa0d1421b9a4992901a9789661ec67ca7b8514e84fb5602615f61ada'
            'e08d0bc5b7e562f5de6998060e993eddada96d93105384960207f7bdf2e1ed6e'
            'fa474d001301b48f15cbfee6ee76987401e93fb3f329a34fb0639c297df4657d')
validpgpkeys=('14F26682D0916CDD81E37B6D61B7B526D98F0353') # Mozilla Software Releases <release@mozilla.com>

# Google API keys (see http://www.chromium.org/developers/how-tos/api-keys)
# Note: These are for Arch Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact foutrelis@archlinux.org for
# more information.
_google_api_key=AIzaSyDwr302FpOSkGRpLlUpPThNTDPbXcIn_FM

# Mozilla API keys (see https://location.services.mozilla.com/api)
# Note: These are for Arch Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact heftig@archlinux.org for
# more information.
_mozilla_api_key=e05d56db0a694edc8b5aaebda3f2db6a

prepare() {
  mkdir mozbuild
  cd firefox-$pkgver

  #https://aur.archlinux.org/packages/firefox-appmenu nicman32 comment 2021-08-16
  patch -Np1 -i ../fix-wayland-build.patch

  # globalmenu patch:
  # to support globalmenu a patch from ubuntu is applied
  # source:
  # http://bazaar.launchpad.net/~mozillateam/firefox/firefox-trunk.head
  # /view/head:/debian/patches/unity-menubar.patch
  patch -Np1 -i ../unity-menubar.patch

  #fix csd window buttons patch
  patch -Np1 -i ../fix_csd_window_buttons.patch

  patch -Np1 -i ../0001-Revert-Bug-1817071-Remove-moz-image-region-support-f.patch

  echo -n "$_google_api_key" >google-api-key
  echo -n "$_mozilla_api_key" >mozilla-api-key

  cat > ../mozconfig <<END
ac_add_options --enable-application=browser
mk_add_options MOZ_OBJDIR=${PWD@Q}/obj

ac_add_options --prefix=/usr
ac_add_options --enable-release
ac_add_options --enable-hardening
ac_add_options --enable-optimize
ac_add_options --enable-rust-simd
ac_add_options --enable-linker=lld
ac_add_options --disable-elf-hack
ac_add_options --disable-bootstrap
ac_add_options --with-wasi-sysroot=/usr/share/wasi-sysroot

# Branding
ac_add_options --enable-official-branding
ac_add_options --enable-update-channel=release
ac_add_options --with-distribution-id=org.archlinux
ac_add_options --with-unsigned-addon-scopes=app,system
ac_add_options --allow-addon-sideload
export MOZILLA_OFFICIAL=1
export MOZ_APP_REMOTINGNAME=${_pkgname//-/}

# Keys
ac_add_options --with-google-location-service-api-keyfile=${PWD@Q}/google-api-key
ac_add_options --with-google-safebrowsing-api-keyfile=${PWD@Q}/google-api-key
ac_add_options --with-mozilla-api-keyfile=${PWD@Q}/mozilla-api-key

# System libraries
ac_add_options --with-system-nspr
ac_add_options --with-system-nss

# Features
ac_add_options --enable-alsa
ac_add_options --enable-jack
ac_add_options --enable-crashreporter
ac_add_options --disable-updater
ac_add_options --disable-tests
END
}

build() {
  cd firefox-$pkgver

  export MOZ_NOSPAM=1
  export MOZBUILD_STATE_PATH="$srcdir/mozbuild"
  export MOZ_ENABLE_FULL_SYMBOLS=1
  export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=pip

  # LTO needs more open files
  ulimit -n 4096

  # Do 3-tier PGO
  echo "Building instrumented browser..."
  cat >.mozconfig ../mozconfig - <<END
ac_add_options --enable-profile-generate=cross
END
  ./mach build

  echo "Profiling instrumented browser..."
  ./mach package
  LLVM_PROFDATA=llvm-profdata \
    JARLOG_FILE="$PWD/jarlog" \
    xvfb-run -s "-screen 0 1920x1080x24 -nolisten local" \
    ./mach python build/pgo/profileserver.py

  stat -c "Profile data found (%s bytes)" merged.profdata
  test -s merged.profdata

  stat -c "Jar log found (%s bytes)" jarlog
  test -s jarlog

  echo "Removing instrumented browser..."
  ./mach clobber

  echo "Building optimized browser..."
  cat >.mozconfig ../mozconfig - <<END
ac_add_options --enable-lto=cross
ac_add_options --enable-profile-use=cross
ac_add_options --with-pgo-profile-path=${PWD@Q}/merged.profdata
ac_add_options --with-pgo-jarlog=${PWD@Q}/jarlog
END
  ./mach build

  echo "Building symbol archive..."
  ./mach buildsymbols
}

package() {
  cd firefox-$pkgver
  DESTDIR="${pkgdir}" ./mach install

  local vendorjs="${pkgdir}/usr/lib/${_pkgname}/browser/defaults/preferences/vendor.js"
  install -Dvm644 /dev/stdin "$vendorjs" <<END
// Use LANG environment variable to choose locale
pref("intl.locale.requested", "");

// Use system-provided dictionaries
pref("spellchecker.dictionary_path", "/usr/share/hunspell");

// Disable default browser checking.
pref("browser.shell.checkDefaultBrowser", false);

// Don't disable extensions in the application directory
pref("extensions.autoDisableScopes", 11);

// Enable GNOME Shell search provider
pref("browser.gnome-search-provider.enabled", true);
END

  local distini="${pkgdir}/usr/lib/${_pkgname}/distribution/distribution.ini"
  install -Dvm644 /dev/stdin "$distini" <<END
[Global]
id=archlinux
version=1.0
about=Mozilla Firefox for Arch Linux

[Preferences]
app.distributor=archlinux
app.distributor.channel=${_pkgname}
app.partner.archlinux=archlinux
END

  local i theme=official
  for i in 16 22 24 32 48 64 128 256; do
    install -Dvm644 browser/branding/$theme/default$i.png \
      "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
  done
  install -Dvm644 browser/branding/$theme/content/about-logo.png \
    "${pkgdir}/usr/share/icons/hicolor/192x192/apps/${pkgname}.png"
  install -Dvm644 browser/branding/$theme/content/about-logo@2x.png \
    "${pkgdir}/usr/share/icons/hicolor/384x384/apps/${pkgname}.png"
  install -Dvm644 browser/branding/$theme/content/about-logo.svg \
    "${pkgdir}/usr/share/icons/hicolor/scalable/apps/${pkgname}.svg"
  install -Dvm644 ../identity-icons-brand.svg \
    "${pkgdir}/usr/share/icons/hicolor/symbolic/apps/${pkgname}-symbolic.svg"

  # Appstream
  install -Dm644 "${srcdir}/org.mozilla.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.mozilla.${pkgname}.metainfo.xml"
  install -Dvm644 ../${_pkgname}.desktop \
    "${pkgdir}/usr/share/applications/org.mozilla.${pkgname/-/_}.desktop"

  # Install a wrapper to avoid confusion about binary path
  install -Dvm755 /dev/stdin "${pkgdir}/usr/bin/${_pkgname}" <<END
#!/bin/sh
exec /usr/lib/${_pkgname}/firefox "\$@"
END

  # Replace duplicate binary with wrapper
  # https://bugzilla.mozilla.org/show_bug.cgi?id=658850
  ln -srfv "${pkgdir}/usr/bin/${_pkgname}" "${pkgdir}/usr/lib/${_pkgname}/firefox-bin"

  # Use system certificates
  local nssckbi="${pkgdir}/usr/lib/${_pkgname}/libnssckbi.so"
  if [[ -e $nssckbi ]]; then
    ln -srfv "${pkgdir}/usr/lib/libnssckbi.so" "$nssckbi"
  fi

  local sprovider="$pkgdir/usr/share/gnome-shell/search-providers/$pkgname.search-provider.ini"
  install -Dvm644 /dev/stdin "$sprovider" <<END
[Shell Search Provider]
DesktopId=$pkgname.desktop
BusName=org.mozilla.${pkgname//-/}.SearchProvider
ObjectPath=/org/mozilla/${pkgname//-/}/SearchProvider
Version=2
END

  export SOCORRO_SYMBOL_UPLOAD_TOKEN_FILE="$startdir/.crash-stats-api.token"
  if [[ -f $SOCORRO_SYMBOL_UPLOAD_TOKEN_FILE ]]; then
    make -C obj uploadsymbols
  else
    cp -fvt "$startdir" obj/dist/*crashreporter-symbols-full.tar.zst
  fi
}
